class Person:
    def __new__(cls, *args, **kwargs):
        print(f"__new__ method of {cls.__name__} calling ...")
        return super().__new__(cls)

    def __init__(self, name):
        print(f"__init__ method of person calling ...")
        self.name = name

    def __str__(self):
        return f"Person({self.name})"

p = Person("John")
print(p)


class Singleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance

s1 = Singleton()
s2 = Singleton()
print(id(s1), id(s2), sep="\n")
print(s1 == s2)

