from time import time


def time_it(func):
    def wrapper(*args, **kwargs):
        start_time = time()
        result = func(*args, **kwargs)
        end_time = time()
        print(
            f"The execution time of {func.__name__} is {end_time - start_time} seconds."
        )
        return result

    return wrapper


def counter(func):
    count = 0

    def wrapper(*args, **kwargs):
        nonlocal count
        count += 1
        print(f"count of calling {func.__name__} is {count}")
        return func(*args, **kwargs)

    return wrapper


def logger(func):
    def wrapper(*args, **kwargs):
        print(f"running {func.__name__}")
        return func(*args, **kwargs)
    return wrapper


@logger
@counter
@time_it
def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)


print(factorial(5))
