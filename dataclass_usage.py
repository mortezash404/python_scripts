from dataclasses import dataclass


class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f"Person(name={self.name}, age={self.age})"

p = Person("Ali", 25)
print(p)

@dataclass
class Person:
    name: str
    age: int

p = Person("Ali", 25)
print(p)
