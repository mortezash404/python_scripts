def sugar(func):
    def inner(*args, **kwargs):
        return func(*args, **kwargs) + 100
    return inner


def milk(func):
    def inner(*args, **kwargs):
        return func(*args, **kwargs) + 200
    return inner


def cream(func):
    def inner(*args, **kwargs):
        return func(*args, **kwargs) + 300
    return inner


def muka(func):
    def inner(*args, **kwargs):
        return func(*args, **kwargs) + 400
    return inner

@sugar
@milk
@cream
def tea(num):
    return num * 1000

@sugar
@milk
@cream
@muka
def coffee(num):
    return num * 1500

print(tea(1))
print(coffee(1))