# normal fibo
def fibo(n):
    if n == 0 or n == 1:
        return 1
    return fibo(n - 1) + fibo(n - 2)


print(fibo(7))

# memoized fibo
memo = {0: 1, 1: 1}

def fibo_memoized(n):
    if n not in memo:
        memo[n] = fibo_memoized(n - 1) + fibo_memoized(n - 2)
    return memo[n]

print(fibo_memoized(7))
