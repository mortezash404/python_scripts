import random

# make limitation for create instance
class MyClass:
    _counter = 0
    def __new__(cls, *args, **kwargs):
        cls._counter += 1
        if cls._counter > 5:
            raise Exception("u exceed the limit")
        return cls

o1 = [MyClass() for _ in range(5)]
print(MyClass._counter)


# make limitation for create instance and return random instance when exceed
class MyClass2:
    _instances = []
    _counter = 0
    def __new__(cls, *args, **kwargs):
        cls._counter += 1
        if cls._counter > 3:
            current_cls = random.choice(cls._instances)
        else:
            current_cls = super().__new__(cls, *args, **kwargs)
            cls._instances.append(current_cls)
        return current_cls

o1 = [ MyClass2() for _ in range(12) ]
print(MyClass2._counter)
print(*[id(i) for i in o1], sep="\n")


