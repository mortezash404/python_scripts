from random import choices
from string import printable

def gen_pass(length):
    return "".join(choices(printable, k=length))

print(gen_pass(8))
