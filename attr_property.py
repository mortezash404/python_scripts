# getter & setter
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        if not isinstance(value, str):
            raise ValueError("name must be string")
        self._name = value

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, value):
        if not isinstance(value, int):
            raise ValueError("age must be int")
        self._age = value

p = Person("ali", 20)

# getter by operator
import operator

class Person2:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    name = property(operator.attrgetter("_name"))

    @name.setter
    def name(self, value):
        if not isinstance(value, str):
            raise ValueError("name must be string")
        self._name = value

    age = property(operator.attrgetter("_age"))

    @age.setter
    def age(self, value):
        if not isinstance(value, int):
            raise ValueError("age must be int")
        self._age = value

p = Person2("Ali", 20)
