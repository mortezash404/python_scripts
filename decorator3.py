# parametrized decorator
import functools

def sugar(number):
    def decorator(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            return func(*args, **kwargs) + 100 * number
        return inner
    return decorator


def milk(number):
    def decorator(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            return func(*args, **kwargs) + 200 * number
        return inner
    return decorator


def cream(number):
    def decorator(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            return func(*args, **kwargs) + 300 * number
        return inner
    return decorator


def muka(number):
    def decorator(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            return func(*args, **kwargs) + 400 * number
        return inner
    return decorator


@sugar(number=2)
@milk(number=3)
@cream(number=1)
def tea(num):
    return num * 1000

print(tea(1))
help(tea) # out without functools.wrap: inner(*args, **kwargs)
help(tea) # out with functools.wrap: tea(num)
