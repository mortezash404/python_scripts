def singleton(class_):
    instances = dict()
    def create(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return create


@singleton
class MyClass:
    pass


o1 = MyClass()
o2 = MyClass()
print(o1 == o2)


class Singleton:
    _instance = None
    def __new__(cls):
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance


o11 = Singleton()
o21 = Singleton()
print(o11 == o21)
